<?php
/* @var $installer RedboxDigital_Linkedin_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$entityTypeId = $installer->getEntityTypeId('customer');
$this->addAttribute('customer', 'linkedin_profile', array(
    'type'      => 'varchar',
    'label'     => 'Linkedin Profile',
    'input'     => 'text',
    'visible'   => true,
    'position'  => 120,
    'required'  => false,//or true
    'is_system' => 0,
    'unique'    => true,
));
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'linkedin_profile');
$attribute->setData('used_in_forms', array(
    'adminhtml_customer',
    'checkout_register',
    'customer_account_create',
    'customer_account_edit',
));
$attribute->setData('is_user_defined', 0);
$attribute->save();

$installer->getConnection()->addColumn(
    $installer->getTable('sales/quote'),
    'linkedin_profile',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'length' => 250,
        'nullable' => true,
        'comment' => 'Customer LinkedIn Profile'
    )
);
$installer->endSetup();
