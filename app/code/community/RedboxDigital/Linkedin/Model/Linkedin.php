<?php
/**
 * LinkedIn model
 *
 * @category    RedboxDigital
 * @package     RedboxDigital_Linkedin
 */
class RedboxDigital_Linkedin_Model_Linkedin extends Mage_Core_Model_Abstract
{
    /**
     * Initializing model
     */
    public function _construct()
    {
        $this->_init('redboxdigital_linkedin/linkedin');
    }
}
