<?php
/**
 * RedboxDigital LinkedIn resource setup model
 *
 * @category    RedboxDigital
 * @package     RedboxDigital_Linkedin
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class RedboxDigital_Linkedin_Model_Resource_Setup extends Mage_Customer_Model_Resource_Setup
{

}
