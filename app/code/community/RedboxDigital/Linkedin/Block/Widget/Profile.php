<?php
/**
 * Block to render customer's linkedin_profile attribute
 *
 * @category   RedboxDigital
 * @package    RedboxDigital_Linkedin
 * @author     RedboxDigital
 */
class RedboxDigital_Linkedin_Block_Widget_Profile extends Mage_Customer_Block_Widget_Abstract
{
    /**
     * Initialize block
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('redboxdigital/customer/widget/profile.phtml');
    }

    /**
     * Check if linkedin_profile attribute enabled in system
     *
     * @return bool
     */
    public function isEnabled()
    {
        return (bool) $this->_getAttribute('linkedin_profile')->getIsVisible();
    }

    /**
     * Check if linkedin_profile attribute marked as required
     *
     * @return bool
     */
    public function isRequired()
    {
        return (bool)$this->_getAttribute('linkedin_profile')->getIsRequired();
    }

    /**
     * Get current customer from session
     *
     * @return Mage_Customer_Model_Customer
     */
    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }
}
